# *DEPRECATED: packages*

# Base runit (chroot)
**CHECK BOOTLOADER!!!** Using efibootmgr, swap partition (check [install-void.md](install-void.md) for swapfile).

Login: `root:artix`

    # cfdisk -z /dev/sda  
    
    # mkswap /dev/sda1
    # mkfs.ext4 /dev/sda2
    # mkfs.vfat -F 32 /dev/sda3
    
    # swapon /dev/sda1
    # mount /dev/sda2 /mnt
    # mkdir /mnt/boot
    # mount /dev/sda3 /mnt/boot
    
    # pacman -Syy
    # basestrap /mnt base base-devel runit elogind-runit
    # basestrap /mnt linux(-zen) linux-firmware
    
    # fstabgen -U /mnt >> /mnt/etc/fstab
    # artools-chroot /mnt
    
    # ln -sf /usr/share/zoneinfo/REGION/COUNTRY/CITY /etc/localtime
    # hwclock --systohc
    
    # pacman -Sy vim
    # vim /etc/locale.gen
    en_US.UTF-8
    en_US ISO-8859-1
    # locale-gen
    # echo 'export LANG="en_US.UTF-8"' >> /etc/locale.conf
    # echo 'export LC_COLLATE="C"' >> /etc/locale.conf
    
    # pacman -Sy efibootmgr
    # blkid
    # efibootmgr --disk /dev/sda --part 3 --create --label "Artix Linux" --loader vmlinuz-linux --unicode 'root=PARTUUID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX rw initrd=initramfs-linux.img' --verbose
    
    # passwd
    # useradd -m USER -G adm,ftp,games,http,log,rfkill,sys,wheel,lp,input,optical,scanner,storage,video
    # passwd USER
    # echo "HOSTNAME" > /etc/hostname
    
    # pacman -S dhcpcd connman-runit connman-gtk
    
    # exit
    # umount -R /mnt
    # reboot
    AFTER REBOOT
    # ln -s /etc/runit/sv/connmand /run/runit/service
    # vim /etc/sudoers
    
## Packages

    xorg-server xorg-xinit xf86-video-dummy xf86-input-void xf86-video-intel xf86-input-synaptics xf86-input-libinput
    intel-ucode mesa vulkan-mesa-layer vulkan-intel vulkan-icd-loader
    wget unrar git termite xmonad xmonad-contrib xmonad-utils xmobar

