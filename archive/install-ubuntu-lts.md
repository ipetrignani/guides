# Ubuntu LTS
Ex Pop!\_OS.

*The New Vim Order*

## First Boot
Xorg (default) for now.

- Browse **Settings**
- Enable Write Cache
- Configure **Software Updater**
- Disable Update Notifier

      echo 'X-GNOME-Autostart-enabled=false' |
      sudo tee -a /etc/xdg/autostart/update-notifier.desktop
- Disable Snaps

      snap list

      sudo snap remove snap-store
      sudo snap remove gtk-common-themes
      sudo snap remove gnome-3-34-1804
      sudo snap remove core18
      sudo snap remove snapd

      sudo apt-get purge snapd
- Reboot

## Setup
- Connect to WiFi
- Open Terminal (Ctrl+Alt+T):
```sh
sudo apt-get update && sudo apt-get dist-upgrade
```
- Install
```
build-essential
curl
ffmpeg
git
gnome-tweaks
mpv
p7zip-rar
vim-gtk
```
- Configure **Files**
- Configure **Firefox**
  - Get **Bitwarden, Dark Reader, uBlock, Vimium-FF**
- Configure **Extensions + Gnome Tweaks**
- Reboot

