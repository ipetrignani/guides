# *DEPRECATED: packages*

# Netinstall
**Deprecated: repositories too old.**

## No Install Recommends
`echo "APT::Install-Recommends \"no\";" > /etc/apt/apt.conf.d/norecommends`

## Network
1. `wpa_passphrase <ssid> [passphrase] >> /etc/wpa_supplicant/wpa_supplicant.conf`
2. `echo "iface eth0 inet manual" > /etc/network/interfaces.d/eth0.conf`
3. `vi /etc/network/interfaces.d/wlan0.conf`
```sh
iface wlan0 inet dhcp
wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
```
4. `ifup wlan0`

## Packages
General:

```
alsa-utils
apulse
build-essential
curl
default-jre
elogind
ffmpeg
git
htop
imagemagick
libdbus-glib-1-2
libgtk-3-0
libpam-elogind
mksh
p7zip-full
p7zip-rar
vim
xserver-xorg-core
xserver-xorg-input-void
xserver-xorg-input-libinput
xserver-xorg-video-dummy
x11-xserver-utils
xinit
```

Specific:
```
brightnessctl
firmware-atheros
firmware-misc-nonfree
intel-microcode
libgl1-mesa-dri
mesa-utils
```

## Suckless Build
```
libx11-dev libxext-dev libxft-dev libxinerama-dev
```

### Change Shell
`chsh -s /bin/mksh`, `chsh -s /bin/mksh dev`, remove home files, exit to USER.

### Installs
* Clone dotfiles.  
* `sh rice1/install.sh`
* Reboot.
* Internet before X!

## Emacs Build
```
discount
emacs-gtk
fd-find
ripgrep
shellcheck
```

### Change Shell
`chsh -s /bin/mksh`, `chsh -s /bin/mksh dev`, remove home files, reboot.

### Alsamixer
Unmute Master, capture Internal Mic (SPC: select, ESC: exit).

### Installs
* `mkdir -p .local/share/xorg`
* Internet: `su - -c 'ifup wlan0'`
* Git clone dotfiles.  
```sh
cp rice1/.gitconfig .
cp rice1/.mkshrc .
cp rice1/.xinitrc-exwm .xinitrc
```
* As root:
```sh
cp rice1/etc/sysctl.d/* /etc/sysctl.d/
cp rice1/etc/udev/rules.d/* /etc/udev/rules.d/
cp -r rice1/etc/X11/xorg.conf.d/ /etc/X11/
```
* Doom Emacs: `git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d`
* [Emacs Install](doom-emacs.md)

## Customizing
- Vimix GTK theme + Papirus Icon theme + papirus-folders
- Slim themes (gnu-dark), `sddm-theme-breeze`
- `gsettings set org.gnome.desktop` for theme
    - `interface gtk-theme`
    - `interface icon-theme`
    - `wm.preferences theme`
- `sudo cp /usr/share/applications/vim.desktop ~/.local/share/applications/`
    - Terminal=false

