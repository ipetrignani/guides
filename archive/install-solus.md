# Solus
Budgie.

*Now adapted for The New Order of VSCode!*

## First boot
- Connect to WiFi
- Upgrade
```sh
sudo eopkg up
```
- Reboot

## Setup
```sh
sudo eopkg it -c system.devel
sudo eopkg it git p7zip vscode

sudo eopkg dc

gsettings set org.gnome.desktop.input-sources xkb-options [\'compose:ralt\']
gsettings set org.gnome.desktop.peripherals.mouse accel-profile flat

sudo nano /etc/systemd/logind.conf

HandleLidSwitch=ignore
```
- Enable write cache
- Configure **Settings, Software Center, Files, Firefox**
- Configure **Budgie Desktop Settings**
- Reboot

