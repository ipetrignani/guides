#+title: Todo

* TODO [0%]
** [ ] Build Knowledge Base
Contains links, programs, games, etc.
*** Links
https://www.technologizer.com/2012/01/23/why-history-needs-software-piracy/
https://www.timj.co.uk/computing/politics-policy/drm/fallacy/
https://github.com/roboyoshi/datacurator-filetree
**** Source: Google Drive
***** Drawing AI
https://www.reddit.com/r/artificial/comments/r8z00s/are_there_any_good_entirely_free_texttoimage_ai/
https://www.reddit.com/r/bigsleep/comments/q238w2/fox_at_night_2_images_made_using_the_new_cogview/
https://www.reddit.com/user/Wiskkey/comments/p2j673/list_part_created_on_august_11_2021/
https://www.reddit.com/r/MediaSynthesis/comments/r04he3/nvidia_releases_web_app_for_gaugan2_which/
https://www.reddit.com/r/bigsleep/comments/s32ake/what_are_the_best_free_online_ai_image_generators/
https://www.reddit.com/r/coolguides/comments/w83fg1/comparison_of_ai_texttoimage_generators/
https://www.reddit.com/r/ArtificialInteligence/comments/rvapvj/where_are_some_free_ai_generators_that_i_could/
***** Torrent
https://piratebay.org
https://xrel.to
https://1337x.to
https://dats.site
https://ripped.guide
***** Github
https://github.com/Igglybuff/
https://gitlab.com/ck-s-technology-news/privacy-tools-list-by-cktn/
https://github.com/mayfrost/
https://github.com/redcroft/
https://github.com/azimstech/
https://github.com/addy-dclxvi/
https://github.com/vinceliuice/
https://github.com/kinleyd/
https://gitlab.com/ambrevar/
https://www.unixsheikh.com/
https://dev.to/
https://gitlab.com/bit9tream/
https://madaidans-insecurities.github.io/
https://github.com/drishal/dotfiles
https://gist.github.com/Prajjwal/2226c6a96d1d72abc713e889160a9f81
https://github.com/pluja/awesome-privacy
***** Activation
https://github.com/massgravel/
***** Sublime Text
https://gist.github.com/jn0/6bcbc3f00e39efe76e0a52de423f30e7/
***** Archive
http://the-eye.eu/
***** Learning
http://drmarkwomack.com/mla-style/document-format/
Ruby, Vim Koans
https://curlie.org/
https://gamefaqs.com/
https://www.reddit.com/r/Bitwarden/comments/10w1i7k/folder_categoriestitles_suggestions/
***** BewareIRCD
http://books.gigatux.nl/mirror/irchacks/059600687X/irchks-CHP-15-SECT-4.html
***** Vim
https://github.com/johndgiese/dotvim/
http://vim-latex.sourceforge.net/
https://blog.interlinked.org/tutorials/vim_tutorial.html/
https://sanctum.geek.nz/arabesque/category/vim/
https://www.256kilobytes.com/storage/infographics/vim-cheat-sheet.png
https://dn.ht/intermediate-vim/
https://blog.petrzemek.net/2016/04/06/things-about-vim-i-wish-i-knew-earlier/
http://www.viemu.com/a-why-vi-vim.html
http://www.viemu.com/a_vi_vim_graphical_cheat_sheet_tutorial.html

- [Arabesque](https://blog.sanctum.geek.nz/)
- [Recommendations](https://www.vi-improved.org/recommendations/)
- [The Patient Vimmer](https://romainl.github.io/the-patient-vimmer/)
- [The Dharma of Vi](https://blog.samwhited.com/2015/04/the-dharma-of-vi/)

- [`ale`](https://github.com/dense-analysis/ale/)
- [`vim-commentary`](https://github.com/tpope/vim-commentary/)
- [`vim-flagship`](https://github.com/tpope/vim-flagship/)
- [`vim-fugitive`](https://github.com/tpope/vim-fugitive/)
- [`vim-repeat`](https://github.com/tpope/vim-repeat/)
- [`vim-sleuth`](https://github.com/tpope/vim-sleuth/)
- [`vim-surround`](https://github.com/tpope/vim-surround/)
- [`vim-vinegar`](https://github.com/tpope/vim-vinegar/)
***** Solitaire
cards.dll - a fun dll that came with windows
***** Music
https://www.reddit.com/r/Metal101/comments/f13egn/i_used_to_listen_to_metal_in_high_school_and/
https://www.reddit.com/r/Metal101/comments/ecj707/i_made_a_thrash_metal_playlist_for_beginners/
https://www.reddit.com/r/Metal101/comments/cq3mpj/looking_for_very_specific_new_music/
https://www.reddit.com/r/spotify/comments/7f5hch/looking_for_rockmetal_without_vocals/
https://www.reddit.com/r/Music/comments/2l27aq/metal_fans_suggestions_for_instrumental_bands/
https://www.reddit.com/r/postmetal/comments/8id9lr/i_prefer_post_metal_without_vocals_anyone_else/
https://www.reddit.com/r/Metal/comments/qev18/good_blackdeath_metal_bands_with_no_vocals/

# Programs

00325-80796-41797-AAOEM
6H32N-C8V87-Y96XW-DJ2GD-7T9QQ

Aseprite
Audacity
BewareIRCd
ConEmu
Coretemp64
Daemon Tools
DPGraph
Everything
F.lux
Far Manager
FastCopy
GeoGebra
Gimp
GNUplot
GrafEQ
Graph4.4.2
GraphicsGale
Greenshot
IDM
Inkscape
Jircii
k-Meleon
K3Dsurf
Krita
MP3tag
MPC-HC
MyDefrag
NirSoft (collection)
Notepad++
RegCleanr
Rufus
SoulseekQT
SumatraPDF
Sysinternals (collection)
TeamSpeak (kernel_daemon @ UN Player)
TeXworks
Tixati
TPCOptimizer
Vim
WhatsApp Viewer
Win32dasm
Winamp
WinDirStat
Wings3D
WinMerge
WinPlot

Fortnite: kazereyema@youzend.net
**** Linux
[[https://www.reddit.com/r/archlinux/comments/18zy8ed/nvme_best_practices/][NVMe Best Practices]]

***** games
[[https://github.com/YoteZip/LinuxCrackingBible][Linux Cracking Bible]]
https://anarc.at/
***** Void
Tweaking my Void setup
xahlee, kkovacs.eu
Troubleshooters

https://animeshz.github.io/site/
http://ratfactor.com/dwm.html
***** Fedora
https://docs.openstack.org/image-guide/fedora-image.html
https://gist.github.com/amitsaha/c407fd2351d1bc555666
***** NixOS
https://github.com/tolgaerok
https://github.com/hlissner
https://stop-using-nix-env.privatevoid.net/

***** FFMpeg
https://trac.ffmpeg.org/wiki/Capture/Desktop#Linux
+ [H.264](https://trac.ffmpeg.org/wiki/Encode/H.264/)
+ [libaom AV1](https://trac.ffmpeg.org/wiki/Encode/AV1/)
+ [Encoding video for the web](https://gist.github.com/Vestride/278e13915894821e1d6f/)
+ [A quick guide to using FFmpeg to create cross-device web videos](https://gist.github.com/jaydenseric/220c785d6289bcfd7366/)
***** GnuPG/Pass
https://www.gnupg.org/
https://www.gnupg.org/documentation/guides.html)

https://www.passwordstore.org/

pass + pass-import

https://git.zx2c4.com/password-store/tree/contrib/dmenu/

xdotool

#+BEGIN_SRC sh
#!/usr/bin/env bash

shopt -s nullglob globstar

typeit=0
if [[ $1 == "--type" ]]; then
	typeit=1
	shift
fi

xdotool="xdotool type --clearmodifiers --file -"

prefix=${PASSWORD_STORE_DIR-~/.password-store}
password_files=( "$prefix"/**/*.gpg )
password_files=( "${password_files[@]#"$prefix"/}" )
password_files=( "${password_files[@]%.gpg}" )

password=$(printf '%s\n' "${password_files[@]}" | dmenu "$@" -fn Terminus:size=12)

[[ -n $password ]] || exit

if [[ $typeit -eq 0 ]]; then
	pass show -c "$password" 2>/dev/null
else
	pass show "$password" | { IFS= read -r pass; printf %s "$pass"; } | $xdotool
fi
#+END_SRC
**** Windows
# Windows 10 LTSC 2019

    en_windows_10_enterprise_ltsc_2019_x64_dvd_5795bb03.iso
    SHA1SUM: 615A77ECD40E82D5D69DC9DA5C6A6E1265F88E28

- Linux: format as `DOS/FAT32`, mount ISO, mount USB, copy contents.
- `$OEM$\$$\Setup\Scripts\SetupComplete.cmd` from MDL's Telemetry Repository.

## First Boot

- Clean taskbar
- Browse **Settings**
- Browse **Control Panel**
- Turn off hibernation
  - CMD (administrator): `powercfg -h off`

My Digital Life
Virtual Customs
Cybermania.ws
AiOwares
en_windows_10_enterprise_2015_ltsb_x64_dvd_6848446.iso
264D48C902E6B586A1ED965062F7DA4D4DA99B35
en_windows_10_enterprise_2016_ltsb_x64_dvd_9059483.iso
031ED6ACDC47B8F582C781B039F501D83997A1CF
en_windows_10_enterprise_ltsc_2019_x64_dvd_5795bb03.iso
615A77ECD40E82D5D69DC9DA5C6A6E1265F88E28

https://gist.github.com/Overimagine1/a0d8b2a98eaf2839d07a0f5c2bd48075
https://rentry.co/activatewin10
https://rentry.co/MR-W10
https://github.com/massgravel/Microsoft-Activation-Scripts/releases
https://massgrave.dev/index.html
https://rentry.co/LTSC#a-note-on-windows-versions
https://github.com/gdeliana/Optimize-Offline#about-optimize-offline
https://www.heidoc.net/php/myvsdump_details.php?id=p8166f112307ax64len
https://archive.org/details/en-us_windows_10_iot_enterprise_ltsc_2021_x64_dvd_257ad90f_202301
https://rentry.co/installwindows#ltsc
https://aopc.dev/t/windows-10-ltsc-the-best-windows-10-version-ever.845/
https://rentry.org/uninstalledge
https://bobpony.com/
https://sha1.rg-adguard.net/
https://www.heidoc.net/php/myvsdump.php

**** Wallpaper
https://earthobservatory.nasa.gov/collection/1658/earth-from-afar
https://science.nasa.gov/earth/multimedia/
https://explorer1.jpl.nasa.gov/galleries/earth-from-space/
https://www.nasa.gov/image-article/waning-gibbous-moon-above-earths-horizon-2/
https://www.orlandosentinel.com/2022/04/22/pictures-earth-seen-from-space/
https://eol.jsc.nasa.gov/
https://wallhaven.cc/w/k91x7q
https://wallhaven.cc/w/3lo8md
https://wallhaven.cc/w/85mk1k ⭐
https://wallhaven.cc/w/85mgvj
https://wallhaven.cc/w/yxo5qk
https://wallhaven.cc/w/rr1g27 ⭐
https://wallhaven.cc/w/gpg5wl
https://wallhaven.cc/w/gpg5wd
https://wallhaven.cc/w/9dp2dx
https://wallhaven.cc/search?categories=110&purity=110&topRange=6M&sorting=toplist&order=desc&ai_art_filter=1
https://wallhaven.cc/w/zyj28v ⭐
https://wallhaven.cc/w/6dg7ll
https://wallhaven.cc/w/jxqyrq
https://wallhaven.cc/w/jxrrmp
https://wallhaven.cc/w/jxqok5
https://wallhaven.cc/w/jxqrw5
https://wallhaven.cc/w/x621xo
https://wallhaven.cc/w/yxwvvg
https://wallhaven.cc/w/rrzp5q
https://wallhaven.cc/w/858lz1 ⭐
https://wallhaven.cc/w/2ywd3y
https://wallhaven.cc/w/2yjp6x
https://wallhaven.cc/w/3lepy9
https://wallhaven.cc/w/zyj8gw
https://wallhaven.cc/w/x6yzoz
https://wallhaven.cc/w/eywqvw
https://wallhaven.cc/w/p9gr2p
https://wallhaven.cc/w/7286p3 ⭐
https://wallhaven.cc/w/j32poq
**** Emacs
- https://emacsrocks.com/
- http://whattheemacsd.com/
*** Fonts
[Practical Typography](https://practicaltypography.com/)

- [M+ FONTS](https://mplusfonts.github.io/)
- [Input Fonts](https://input.djr.com/)
- [Source Code Pro ("most readable")](https://adobe-fonts.github.io/source-code-pro/)
- [Fira Code (95% SCP, best with ligatures)](https://github.com/tonsky/FiraCode/)
- [Iosevka (best slender type)](https://typeof.net/Iosevka/)
- Bitmaps: Dina, Cozette, Terminus
- Nerd Fonts (?)
- Spleen
- Proggyfonts
- Gohufont
- Dina
- Tamzen, Tamsyn
- Terminus, Termsyn
- Unifont
- [[https://skinwalker.wordpress.com/2012/01/20/dweep-x11-font-mod-of-neep/][Dweep: Mod of Neep]]
- Bitocra
- Creep
- Github: 'bitmap-fonts' repos.
** [ ] Suckless: revise Rocks
[[https://www.reddit.com/r/unixporn/comments/pemeag/dwm_finally_got_dwm_patched/][Example]]
- Deepen DWM understanding (e.g. colors)
** [ ] Create Gitlab Account
** [ ] Cleanup Git: devoid/
** [ ] Cleanup: $HOME
** [ ] Update Git: devoid/
** [ ] Update: $HOME?
** [ ] Update Git: .suckless/
** [ ] QEMU: Void Linux
** [ ] QEMU: NixOS
** [ ] Emacs: DistroTube
** [ ] Emacs: Zaiste DoomCasts
** [ ] Emacs: Seorenn
** [ ] Emacs: Gavin Freeborn
** [ ] Emacs: System Crafters
** [ ] Vim: Nir Lichtman
** [ ] Vim: Vimtutor
