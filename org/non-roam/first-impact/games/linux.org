#+title: Info

* :toc_1:
- [[#goldberg-emu][Goldberg Emu]]
- [[#armagetron-advanced][Armagetron Advanced]]
- [[#astral-ascent-astral-ascent][Astral Ascent (~/Astral Ascent)]]
- [[#cataclysm-dark-days-ahead][Cataclysm: Dark Days Ahead]]
- [[#crimsonland][Crimsonland]]
- [[#crusader-kings-ii][Crusader Kings II]]
- [[#darkest-dungeon-localshare-red-hook-studios][Darkest Dungeon (.local/share: Red Hook Studios/)]]
- [[#darkest-dungeon-ii][Darkest Dungeon II]]
- [[#doom-3][Doom 3]]
- [[#dwarf-fortress-save-prefs][Dwarf Fortress (save/, prefs/)]]
- [[#elona][Elona]]
- [[#endless-sky][Endless Sky]]
- [[#factorio][Factorio]]
- [[#flare][Flare]]
- [[#freeciv][Freeciv]]
- [[#half-life-2-hl2save-episode-one-episodicsave-episode-two-ep2save][Half-Life 2 (hl2/save/), Episode One (episodic/save/), Episode Two (ep2/save/)]]
- [[#heroic][Heroic]]
- [[#igowin][Igowin]]
- [[#left-4-dead-2][Left 4 Dead 2]]
- [[#minecraft][Minecraft]]
- [[#nethack][Nethack]]
- [[#openttd-config-openttd-localshare-openttd][OpenTTD (.config: openttd, .local/share: openttd)]]
- [[#oolite][Oolite]]
- [[#pegasus][Pegasus]]
- [[#rimworld-configunity3dludeon-studiosrimworld-by-ludeon-studios][RimWorld (.config/unity3d/Ludeon Studios/RimWorld by Ludeon Studios)]]
- [[#stardew-valley][Stardew Valley]]
- [[#starsector][Starsector]]
- [[#steamstub-remover][Steamstub Remover]]
- [[#stellaris][Stellaris]]
- [[#stellarium][Stellarium]]
- [[#survival-crisis-z][Survival Crisis Z]]
- [[#teamspeak][TeamSpeak]]
- [[#the-powder-toy][The Powder Toy]]
- [[#terraria][Terraria]]
- [[#the-powder-toy-1][The Powder Toy]]
- [[#unreal-tournament-2004][Unreal Tournament 2004]]
- [[#warzone-2100][Warzone 2100]]
- [[#worms-armageddon][Worms: Armageddon]]

* [[https://mr_goldberg.gitlab.io/][Goldberg Emu]]
** Experimental version
Has nice overlay (S-TAB).
** Server from SteamCMD anonymous?
*** Update
- =account_name.txt=
- =user_steam_id.txt= at =Goldberg SteamEmu Saves/settings=
  after running the game once
** Instructions for LAN
*** Host
- =listen_port.txt= must be the same for all users.
- Port forward unless =listen_port.txt= has been changed.
  - For now, not necessary (firewall accepts conn. related to conn. created by us)
** Instructions for WAN
*** All players
- Create =disable_lan_only.txt= beside =steam_api(64).dll=
*** Guests (outside LAN)
- Create folder =steam_settings= beside =steam_api(64).dll=
  - Place host IP in =custom_broadcasts.txt=.
** Original post
I just want to point out that although I see plenty of posts suggesting that the Goldberg Emu only works over LAN and requires a VPN like Hamachi or ZeroTier to play with friends over the internet, this is simply not true. The Goldberg Emu works fine over the internet if properly setup, and I find it preferable to Steamworks cracks and the SSE. I've included some instructions below for those who are interested.

These steps assume the Goldberg Emu has been properly installed. I always use the latest build from https://mr_goldberg.gitlab.io/goldberg_emulator/, and apply the version in the "experimental" folder (since it has the nice Shift-Tab overlay). I also advise my friends to update the files account_name.txt and user_steam_id.txt at %appdata%\Goldberg SteamEmu Saves\settings after running the game once, to avoid any username/ID conflict issues.

1. For all players, a file called "disable_lan_only.txt" must be created beside the Goldberg Emu steam_api64.dll (so, for Valheim, at valheim_Data\Plugins\x86_64). It can be left empty.
2. Whoever is hosting the world will need to forward the right port, usually 47584 unless listen_port.txt has been changed (listen_port.txt must be the same for all users).
3. Whoever is connecting to the host outside of the LAN will need to create a folder called "steam_settings" beside steam_api64.dll (same directory as "disable_lan_only.txt"). Inside this folder, a file called "custom_broadcasts.txt" must be created. The public IP address or domain of the host must be placed inside of this file.

That's it. The host will be able to see all the clients in the Shift-Tab overlay, but the clients won't be able to see each other. However, they will all be able to join the host's world and play normally thereon.

This works with the Valheim headless server too. (Interestingly, it also seems that no further ports need forwarding when using the Goldberg Emu.) I recommend you download it anonymously from Steam using SteamCMD (there are plentiful tutorials for doing this). Keep the server files separate from the client, and apply the Goldberg Emu the same way as any client. When the server has been started from the batch script, it will appear for the clients under Community in the Select Server menu. Remember that the clients need to place the IP/domain for the dedicated server in that custom_broadcasts.txt file, or they won't see it.
* Armagetron Advanced
* Astral Ascent (~/Astral Ascent)
Eats memory, wee! [[https://github.com/ptitSeb/box86-compatibility-list/issues/442][Issue #442]]
* Cataclysm: Dark Days Ahead
** deps
+ SDL2_mixer
+ SDL2_ttf
* Crimsonland
* Crusader Kings II
** deps
+ tbb-2020.3
* Darkest Dungeon (.local/share: Red Hook Studios/)
* Darkest Dungeon II
* Doom 3
** deps
+ Dhewm 3
+ alsa-oss
+ alsa-oss-32bit
** install
Mount the image via loopback device
> mount -t iso9660 doom3-CD1.iso /mnt/cdrom/ -o loop

Install the linux client
> sh /mnt/cdrom/doom3-linux-1.1.1286.x86.run

Now, install the game files
> cd /usr/local/games/doom3/base
> tar -xjvf /mnt/cdrom/files-1.tar.bz2
> umount /mnt/cdrom

Do the same for the remaining images
> mount -t iso9660 /path/to/doom3-CD2.iso /mnt/cdrom/ -o loop
> tar -xjvf /mnt/cdrom/files-2.tar.bz2
> umount /mnt/cdrom

> mount -t iso9660 /path/to/doom3-CD3.iso /mnt/cdrom/ -o loop
> tar -xjvf /mnt/cdrom/files-3.tar.bz2
> umount /mnt/cdrom

Now you can execute:

> LD_LIBRARY_PATH="/usr/lib32:$LD_LIBRARY_PATH" aoss ./doom3 +set s_driver oss
* Dwarf Fortress (save/, prefs/)
** deps
+ gtk+
+ libopenal
+ SDL
+ SDL_image
+ SDL_ttf
* Elona
* Endless Sky
* Factorio
* Flare
* Freeciv
* Half-Life 2 (hl2/save/), Episode One (episodic/save/), Episode Two (ep2/save/)
** nosteam
+ Use 32-bit (linux/x86).
* Heroic
** deps
+ libunwind
+ Update dynamic linker cache

  doas mv /etc/ld.so.cache /tmp
  doas ldconfig
  ldconfig -p | grep /usr/lib32/libvulkan.so

  Missing i386: link with =/usr/lib32/vulkan.so.30= instead of just =vulkan.so=
* Igowin
* Left 4 Dead 2
** nosteam
+ Same as hl2 (32-bit: linux/x86).
** deps
+ libopenal-32bit (for engine.so)
+ libpng12.so.0 (for vguimatsurface.so)
  "libpng12-0_1.2.54-1ubuntu1.1_i386.deb"
* Minecraft
** mods
+ Dynamic FPS
+ Cloth Config v14
+ EntityCulling
+ MidnightLib
+ Krypton
+ Sound Physics Remastered
+ ModernFix
+ Visuality
+ Sodium Extra
+ BadOptimizations
+ ImmediatelyFast
+ Fabric API
+ Mod Menu
+ Reese's Sodium Options
+ FabricSkyBoxes
+ Blur+ (Fabric)
+ Lithium
+ FerriteCore
+ Sodium
** server mods
+ Ferrite Core
+ Lithium
+ Krypton
+ Noisium
** opt
+ Better Beds
+ Carpet, Extra
+ Chat-Up
+ Clear Skies
+ Item Scroller
+ Litematica
+ Logical Zoom
+ Malilib
+ Minihub
+ No Fade
+ Tweakeroo
** fabric
#+BEGIN_SRC sh
#!/bin/sh

USAGE="Usage: install-fabric.sh [ options ]
    -c  client
    -s  server"

while getopts cs optchar
do
    case $optchar in
        c)
            echo "Installing Fabric client..."
            echo
            java -jar fabric-installer* client -noprofile -dir /home/petri/.minecraft
            ;;
        s)
            echo "Installing Fabric server..."
            echo
            java -jar fabric-installer* server -dir server -downloadMinecraft
            ;;
        *)
            echo "$USAGE"
            ;;
    esac
done
shift "$(expr $OPTIND - 1)"
#+END_SRC
** misc
- [Xekr's Resource Packs](https://xekr.lanzoui.com/b01bn74zc)
  - Password: `d6dn`
- [Fabric Minigames](https://nucleoid.xyz)
- [Optifine Alternatives](https://lambdaurora.dev/optifine_alternatives)
- [masa's Minecraft mods](https://masa.dy.fi/tmp/minecraft/mods/client_mods)
* Nethack
* OpenTTD (.config: openttd, .local/share: openttd)
** deps
+ timidity
+ freepats
** links
+ [[https://www.tt-forums.net/viewtopic.php?t=3407][TT Forums - List of Download Locations]]
+ [[https://www.tt-forums.net/viewtopic.php?t=1630][TT Forums - Useful Links]]
+ [[https://www.transporttycoon.net/][Owen's Transport Tycoon Station]]
+ [[https://www.nylon.net/ttd/][The Holy Church of Transport Tycoon]]
* Oolite
* Pegasus
+ xcb-util-keysyms
+ xcb-util-renderutil
+ xcb-util-wm
+ libxkbcommon-x11
#+BEGIN_SRC makefile
collection: Windows
launch: sh -c "export SteamAppId=$(cat '{file.dir}/steam_appid.txt') && export STEAM_COMPAT_DATA_PATH=$HOME/games/proton/compatdata && export STEAM_COMPAT_CLIENT_INSTALL_PATH=/ && $HOME/games/proton/GE-Proton9-5/proton waitforexitandrun '{file.path}'"

game: Balatro
file: balatro/Balatro.exe
assets.banner: media/Balatro/banner.jpg

game: Curse of the Dead Gods
file: curseofthedeadgods/steamapps/common/Curse of the Dead Gods/Curse of the Dead Gods.exe
#+END_SRC
* RimWorld (.config/unity3d/Ludeon Studios/RimWorld by Ludeon Studios)
Fucking crashes (SIGSEGV) if Config/Prefs.xml exists.
* Stardew Valley
** deps
+ libssl1.1
* Starsector
** keys
J7AI7-MSYWP-LAVAN-AHA3V
TGA33-P8BJD-A1A5C-M3A6J
TP56Z-YXBAL-E7KGA-OXZO7
7UHAT-VG6AA-C0R84-AT4QS
HADIS-AYNYN-M2U1A-QHAAN
AO8QF-HY2AD-2AE53-U8AYZ
PO02S-LF23C-BNP4G-MMLO7
F9I7L-EJ1NL-ANAAM-R0A0C
A8TMT-AY54V-9A3CA-A1W57
6LOIA-FBNOJ-FDEC8-SFJJY
TXLAA-OAAW4-9MAAX-7NPA8
8JAOA-9EHH6-LRJ9W-MQJWL
TEITW-HP9ON-A7HMK-WA6YA
* Steamstub Remover
unpack.py
** deps
+ python3-pyelftools
+ python3-pycryptodome
* Stellaris
Fries CPU, wee!
* Stellarium
* Survival Crisis Z
* TeamSpeak
** deps
+ xcb-util-keysyms
+ xcb-util-wm
+ libxkbcommon-x11
+ xcb-util-renderutil
* The Powder Toy
* Terraria
** opt
+ All items.rar world
* The Powder Toy
* Unreal Tournament 2004
* Warzone 2100
** opt
+ rtkit
+ gdb
* Worms: Armageddon
