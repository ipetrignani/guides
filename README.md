[[_TOC_]]
```

## \#FontFetish


## [Rails](https://rubyonrails.org/)

### Ctags

    ctags -R --languages=ruby --exclude=.git --exclude=log --exclude=tmp . $(bundle list --paths)

### Gem

- [`bundler`](https://github.com/rubygems/bundler)
- [`rails`](https://github.com/rails/rails)
- [`rails_best_practices`](https://github.com/flyerhzm/rails_best_practices)
- [`rubocop`](https://github.com/rubocop/rubocop)
- [`rubocop-rails`](https://github.com/rubocop/rubocop-rails)
- [`rcodetools`](https://github.com/rcodetools/rcodetools)
- [`solargraph`](https://github.com/castwide/solargraph)

### Bundle

- [`byebug`](https://github.com/deivid-rodriguez/byebug)
- [`devise`](https://github.com/heartcombo/devise)
- [`factory_bot_rails`](https://github.com/thoughtbot/factory_bot_rails)
- [`faker`](https://github.com/faker-ruby/faker)
- [`file_validators`](https://github.com/musaffa/file_validators)
- [`jwt`](https://github.com/jwt/ruby-jwt)
- [`pagy`](https://github.com/ddnexus/pagy)
- [`pry-byebug`](https://github.com/deivid-rodriguez/pry-byebug)
- [`pry-rails`](https://github.com/pry/pry-rails)
- [`pry-rescue`](https://github.com/ConradIrwin/pry-rescue)
- [`ransack`](https://github.com/activerecord-hackery/ransack)
- [`representable`](https://github.com/trailblazer/representable)
  - `multi_json`
- [`swagger-blocks`](https://github.com/fotinakis/swagger-blocks)

### Rubocop

`.rubocop.yml`

```yml
require: rubocop-rails

AllCops:
  Exclude:
    - 'db/**/**'
    - 'docs/**/**'
    - 'vendor/**/**'
    - 'bin/**/**'
    - 'tmp/**/**'
    - 'config/**/**'
  NewCops: enable
```

### Vim

- [`vim-bundler`](https://github.com/tpope/vim-bundler/)
- [`vim-dispatch`](https://github.com/tpope/vim-dispatch/)
- [`vim-endwise`](https://github.com/tpope/vim-endwise/)
- [`vim-rails`](https://github.com/tpope/vim-rails/)
- [`vim-ruby`](https://github.com/vim-ruby/vim-ruby/)
- [`vim-ruby-xmpfilter`](https://github.com/t9md/vim-ruby-xmpfilter/)

`.vimrc`:

```vim
let g:ale_linters = {
\   'ruby': ['rubocop'],
\}

let g:ale_linters_explicit = 1
let g:ale_set_highlights = 0

map <F4> <Plug>(xmpfilter-mark)
map <F5> <Plug>(xmpfilter-run)
```

