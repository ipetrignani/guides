# Windows 10 LTSC 2019

    en_windows_10_enterprise_ltsc_2019_x64_dvd_5795bb03.iso
    SHA1SUM: 615A77ECD40E82D5D69DC9DA5C6A6E1265F88E28

- Linux: format as `DOS/FAT32`, mount ISO, mount USB, copy contents.
- `$OEM$\$$\Setup\Scripts\SetupComplete.cmd` from MDL's Telemetry Repository.

## First Boot

- Clean taskbar
- Browse **Settings**
- Browse **Control Panel**
- Turn off hibernation
  - CMD (administrator): `powercfg -h off`
